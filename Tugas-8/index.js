var readBooks = require('./callback.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 },
];

function recs(waktu, n) {
  readBooks(waktu, books[n], function (sisa) {
    if (books[n] == books[books.length - 1]) {
      return sisa;
    } else if (sisa < books[n + 1].timeSpent) {
      return sisa;
    } else {
      return recs(sisa, n + 1);
    }
  });
}

recs(10000, 0);
