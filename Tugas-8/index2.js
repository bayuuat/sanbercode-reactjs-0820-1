var readBooksPromise = require('./promise.js');

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
];

const rekursif = (waktu, n) => {
    readBooksPromise(waktu, books[n])
        .then((sisa) => {
            rekursif(sisa, n + 1);
        })
        .catch((error) => {});
};

rekursif(9000, 0);
