//NOMOR 1
function halo() {
    return "Halo Sanbers!"
}

console.log(halo())
console.log()

//NOMOR 2
function kalikan(a, b) {
    return a * b
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali)
console.log()

//NOMR 3
function introduce(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
console.log()

//NOMOR 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992]

var obj = {
    nama: arrayDaftarPeserta[0],
    jenisKelamin: arrayDaftarPeserta[1],
    hobi: arrayDaftarPeserta[2],
    tahunLahir: arrayDaftarPeserta[3]
}

console.log(obj)
console.log()

//NOMOR 5
var buah = [
    { nama: "strawberry", warna: "merah", adaBijinya: false, harga: 9000 },
    { nama: "jeruk", warna: "oranye", adaBijinya: true, harga: 8000 },
    { nama: "Semangka", warna: "Hijau & Merah", adaBijinya: true, harga: 10000 },
    { nama: "Pisang", warna: "Kuning", adaBijinya: false, harga: 5000 },
]

console.log(buah[0])
console.log()

//NOMOR 6
var dataFilm = []

function tambahData(data, namaFilm, durasiFilm, genreFilm, tahunFilm) {
    data.push({ nama: namaFilm, durasi: durasiFilm, genre: genreFilm, tahun: tahunFilm })
}

tambahData(dataFilm, 'Jumanji', '02:25:00', 'Adventure', '2017')
tambahData(dataFilm, 'Avatar', '02:45:00', 'Sci-Fi', '2019')
tambahData(dataFilm, 'Avengers', '03:15:00', 'Action', '2012')
console.log(dataFilm)