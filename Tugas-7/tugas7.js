//NOMOR 1 RELEASE 0
class Animal {
  constructor(name) {
    this._name = name;
    this.def_legs = 4;
    this.def_cold_blooded = false;
  }

  get name() {
    return this._name;
  }

  get legs() {
    return this.def_legs;
  }

  set legs(jumlah) {
    this.def_legs = jumlah;
  }

  get cold_blooded() {
    return this.def_cold_blooded;
  }
}

var sheep = new Animal('shaun');

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);
console.log();

//NOMOR 1 RELEASE 1
class Ape extends Animal {
  constructor(name) {
    super(name);
    this.def_legs = 2;
  }

  yell() {
    console.log('Auooo');
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
  }

  jump() {
    console.log('hop hop');
  }
}

var sungokong = new Ape('kera sakti');
sungokong.yell();
console.log(sungokong.name);
console.log(sungokong.legs);
console.log(sungokong.cold_blooded);
console.log();

var kodok = new Frog('buduk');
kodok.jump();
console.log(kodok.name);
console.log(kodok.legs);
console.log(kodok.cold_blooded);
console.log();

//NOMOR 2
class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();
