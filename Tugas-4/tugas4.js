//NOMOR 1
console.log("LOOPING PERTAMA");
var i = 1;
while (i <= 10) {
    console.log(i * 2 + " - I love coding");
    i++;
}

console.log("LOOPING KEDUA");
var j = 10;
while (j > 0) {
    console.log(j * 2 + " - I will become a frontend developer");
    j--;
}
console.log();

//NOMOR 2
for (var i = 1; i <= 20; i++) {
    if (i % 2 == 0) {
        console.log(i + " - Berkualitas");
    } else {
        if (i % 3 == 0) {
            console.log(i + " - I love coding");
        } else {
            console.log(i + " - Santai");
        }
    }
}
console.log();

//NOMOR 3
var temp = ''
for (var i = 1; i <= 7; i++) {
    temp += '#'
    console.log(temp)
} console.log()

//NOMOR 4
var kalimat = "saya sangat senang belajar javascript"

var array = kalimat.split(" ");
console.log(array)
console.log()

//NOMOR 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

daftarBuah.sort()
for (var i = 0; i < daftarBuah.length; i++) {
    var check = daftarBuah[i]
    console.log(check)
}
