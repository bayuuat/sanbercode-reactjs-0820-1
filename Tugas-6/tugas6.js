//NOMOR 1
const luasLingkaran = (jari) => {
    let hasil = 3.14 * jari * jari
    return ('Luas Lingkaran : ' + hasil)
}

const kelilingLingkaran = (jari) => {
    let hasil = 3.14 * jari * 2
    return ('Keliling Lingkaran : ' + hasil)
}

console.log(luasLingkaran(7))
console.log(kelilingLingkaran(7))
console.log()

//NOMOR 2
let kalimat = ""

const tambahKata = (a) => {
    const kata = a

    kalimat += `${kata} `
    return kalimat
}

tambahKata('saya')
tambahKata('adalah')
tambahKata('seorang')
tambahKata('frontend')
tambahKata('developer')
console.log(kalimat)
console.log()

//NOMOR 3
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(firstName + " " + lastName)
            return
        }
    }
}

newFunction("William", "Imoh").fullName()
console.log()

//NOMOR 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject

console.log(firstName, lastName, destination, occupation)
console.log()

//NOMOR 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)
